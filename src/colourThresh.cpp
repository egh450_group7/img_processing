#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <cv.hpp>
#include <opencv2/core/core.hpp>

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <std_msgs/Time.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>


// This script is used as the image processing software for Group 7 EGH450
// It is responsible for conducting all colour thresholding and computations
// that must take place for image detection
//
// Author: Kathryn Dorge


// Define Enums specifying different detections
enum SHAPE_DETECTION
{
    TRIANGLE = 2,
    SQUARE = 1
};


// Define global variables
// image transport publishers
image_transport::Publisher outputImage;
image_transport::Publisher orangeSquareMask;
image_transport::Publisher blueTriangleMask;

// target found publishers
ros::Publisher squareTargetFound;
ros::Publisher triangleTargetFound;

// ros tf2 broad caster
tf2_ros::TransformBroadcaster *tfbr;

// real world points for shapes
// std::vector< cv::Point3f > triangle_coordinate;
std::vector< cv::Point3f > square_coordinate;

// camera projection matrix
//std::vector< double > projection;
// std::vector< float > dist_coefs;
cv::Mat projection( 3, 3, cv::DataType< double >::type );
cv::Mat dist_coefs;

// filtering coefficients for solvepnp
double alpha        = 0.8;
double x_f_square   = 0.0;
double y_f_square   = 0.0;
double x_f_triangle = 0.0;
double y_f_triangle = 0.0;


// Number of frames to get before detection
int n_square_frames   = 0;
int n_triangle_frames = 0;


// Thresholding ranges
#define H_ORANGE_LOW       0
#define H_ORANGE_HIGH      20
#define S_ORANGE_LOW       95
#define S_ORANGE_HIGH      250
#define V_ORANGE_LOW       130
#define V_ORANGE_HIGH      226

#define H_TRIANGLE_LOW     90
#define H_TRIANGLE_HIGH    140
#define S_TRIANGLE_LOW     140
#define S_TRIANGLE_HIGH    280
#define V_TRIANGLE_LOW     100
#define V_TRIANGLE_HIGH    220

// Define shape sizes
#define AREA_MAX_ORANGE    15000
#define AREA_MIN_ORANGE    4000

#define AREA_MAX_TRIANGLE  15000
#define AREA_MIN_TRIANGLE  2000

#define NUM_FRAMES 2

// Define function prototypes

// This function is responsible for receiving an image and performing colour
// thresholding on it in the HSV range. Currently this is configured to
// match that of the orange square Target.
//
// Input:
//    cv::Mat &src
//        - This is a reference to the image undergoing thresholding
//
//    cv::Mat &thresh
//        - This is a reference to the output thresholded image
//
//    SHAPE_DETECTION shape
//        - The shape that is to be located in this frame (orange square
//          or blue triangle)
//
// Output:
//    void
//        - nothing
void getThresholding( cv::Mat &src, cv::Mat &thresh,  SHAPE_DETECTION shape );


// This function is responsible for calculating all the contours within the image
//
// Input:
//    cv::Mat &maskApplied
//        - This is a reference to the image of which already has a mask applied
//          to it from the getThresholding function
//
//    cv::Mat &src
//        - This is a reference to the original source image
//
//    cv::Mat &edges
//        - These are the edges that where calculated in image, this is
//          found from the contours calculated and is used in debugging
//
//    std::vector< std::vector< cv::Point > > &contours
//        - This is a reference to the points of the contours calculated
//
//    std::vector< cv::Vec4i > &hierarchy
//        - This is a reference to the hierarchy calculated, this contains
//          additional information about the contours calculated
//
// Output:
//    void
//        - nothing
void locatingContours( cv::Mat &maskApplied, cv::Mat &src, cv::Mat &edges, std::vector< std::vector< cv::Point > > &contours, std::vector< cv::Vec4i > &hierarchy );


// This function is used to draw the contours the image once a positive detection
// of the target has occured.
//
// Input:
//    cv::Mat &detectedEdges
//        - This is the Mat type containing all of the detected edges which will
//          be drawn
//
//    std::vector< std::vector< cv::Point > > &contours
//        - The calculated contours which will be drawn onto the image
//
//    std::vector< cv::Vec4i > &hierarchy
//        - The hierarchy of the contours calculated, providing additional
//          providing additional information on the contours
//
//    cv::Mat &src
//        - The original source of the image which is to be drawn over with the contours
//
//    SHAPE_DETECTION shape
//        - The type of shape to be detected
//
//    cv::Mat &src
//        - The original source image which is to be drawn on
//
// Output:
//    void
//        - nothing
void drawContours( cv::Mat &detectedEdges, std::vector< std::vector< cv::Point > > &contours, std::vector< cv::Vec4i > &hierarchy, cv::Mat &src, SHAPE_DETECTION shape );


// This function is responsible for performing solvepnp on the located target in order
// to find the transform of the target from that of the camera.
//
// Input:
//    std::vector< std::vector< cv::Point > > &contours
//        - A reference to the calculated contours detected to be the target
//
//    cv::Point &centre
//        - A reference to the calculated centre of the shape
//
//    SHAPE_DETECTION shape
//        - The enum representing what shape is to be detected
//
//    cv::Mat &src
//        - A reference to the original source image of the shape
//
// Output:
//    cv::Mat &tvec
//        - This is an output vector containing the translation of the image
//
//    bool
//        - This is a boolean output representing if the shape was correctly located
bool completeSolvePNP( std::vector< std::vector< cv::Point > > &contours, cv::Point &centre, SHAPE_DETECTION shape, cv::Mat &src, cv::Mat &tvec );

// This function is responsible for performing all computations required to
// detect if the orange square or blue triangle is present within a frame
//
// Input:
//    std::vector< std::vector< cv::Point > > &contours
//        - These are the contours present in the image detected for a certain
//          threshold value
//
//    cv::Mat &src
//        - This is the original source of the frame
//
//    std::vector< std::vector< cv::Point > > &shapeContours
//        - These are the contours of the shape calculated and will be used to
//          determine if the shape exists in image
//
//    SHAPE_DETECTION shape
//        - The shape that is being detected in the current frame
//
// Output:
//    cv::Point &centre
//        - An output point which contains the centre location of the target
// 
//    bool
//        - A boolean value indicating if the square target is located, with
//          true indicating that it is found
bool shapeTargetDetected( std::vector< std::vector< cv::Point > > &contours, cv::Mat &src, std::vector< std::vector< cv::Point > > &shapeContours, SHAPE_DETECTION shape, cv::Point &centre );


// This function is responsible for determining if the detected shape is a target
// 
// Input:
//    std::vector< std::vector< cv::Point > > &contours
//        - This is the calculated contours in the images that was completed after thresholding
//
//    std::vector< std::vector< cv::Point > > &detectedShape
//        - This is the vector representing the detected shape contours
//
//    int count
//        - This is the number of sides the target shape should have
//
//    int maxSize
//        - This is the maximum area in pixels that the detected shape should have
//
//    int minSize
//        - This is the minimum area that the shape should have
// 
//    cv::Point &centre
//        - This is the centre of the shape calculated
void findShapesFromContours( std::vector< std::vector< cv::Point > > &contours, std::vector< std::vector< cv::Point > > &detectedShape, int count, int maxSize, int minSize, cv::Point &centre );

// This function is responsible for performing all necessary steps to determine
// if any shapes are present within the image frame
//
// Input:
//    cv::Mat &src
//        - The original source image
//
//    cv::Mat &maskSrc
//        - The reference to the mask source produced during detection
//
// Output:
//    void
//        - Nothing
void imageProcessing( cv::Mat &src, cv::Mat &maskSquareSrc, cv::Mat &maskTriangleSrc, cv::Mat &tvec_triangle, cv::Mat &tvec_square, bool &triangle_pnp, bool &square_pnp );

// This function is responsible for controlling the steps undertaking when an
// image is detected in the webcam/image_raw node.
//
// Input:
//    const sensor_msgs::Image::ConstPtr &msg
//        - A reference to the image node sent by the node
//
// Output:
//    void
//        - Nothing
void imgCallback( const sensor_msgs::Image::ConstPtr &msg );


// This function is responsible for receiving and storing the camera calibration data
// which is to be used in OpenCV's solvepnp function. It is called if information is 
// published to /webcam/camera_info node
//
// Input:
//    const sensor_msgs::CameraInfo::ConstPtr &msg
//        - A reference to the message sent by the node
//
// Output:
//    void
//        - Nothing
void callbackInfo( const sensor_msgs::CameraInfo::ConstPtr &msg );


// Main entry point to the application
int main( int argc, char **argv )
{
    // Start initialising ROS elements
    ros::init( argc, argv, "img_processor" );

    // transform
    tfbr = new tf2_ros::TransformBroadcaster();

    square_coordinate.push_back( cv::Point3f(  0.0,  0.0, 0.0 ) );
    square_coordinate.push_back( cv::Point3f( -0.1,  0.1, 0.0 ) );
    square_coordinate.push_back( cv::Point3f( -0.1,  0.0, 0.0 ) );
    square_coordinate.push_back( cv::Point3f( -0.1, -0.1, 0.0 ) );
    square_coordinate.push_back( cv::Point3f(  0.0, -1.0, 0.0 ) );
    square_coordinate.push_back( cv::Point3f(  0.1, -0.1, 0.0 ) );
    square_coordinate.push_back( cv::Point3f(  0.1,  0.0, 0.0 ) );
    square_coordinate.push_back( cv::Point3f(  0.1,  0.1, 0.0 ) );
    square_coordinate.push_back( cv::Point3f(  0.0,  0.1, 0.0 ) );


dist_coefs = cv::Mat::zeros( 5, 1, cv::DataType< double >::type );

    // hard code some values 
    projection.at< double >( 0, 0 ) = 986.108154296875;
    projection.at< double >( 0, 1 ) = 0;
    projection.at< double >( 0, 2 ) = 343.9864727946915;
    projection.at< double >( 1, 0 ) = 0;
    projection.at< double >( 1, 1 ) = 986.4599609375;
    projection.at< double >( 1, 2 ) = 255.6404799499433;
    projection.at< double >( 2, 0 ) = 0;
    projection.at< double >( 2, 1 ) = 0;
    projection.at< double >( 2, 2 ) = 1;

    dist_coefs.at< double >(0) = 0.2169447142222751;
    dist_coefs.at< double >(1) = -0.7195743223243192;
    dist_coefs.at< double >(2) =  0.001697731306442505;
    dist_coefs.at< double >(3) = 0.003231111745932941;
    dist_coefs.at< double >(4) = 0.0;

    ros::NodeHandle n;

    ros::Subscriber camera_info_sub = n.subscribe( "/webcam/camera_info", 1000, callbackInfo );

    image_transport::ImageTransport it( n );


    image_transport::Subscriber sub = it.subscribe( "/webcam/image_raw", 1000, imgCallback   );


    outputImage      = it.advertise( "/image_processing/output", 1 );
    orangeSquareMask = it.advertise( "/image_processing/target_one_thresh", 1 );
    blueTriangleMask = it.advertise( "/image_processing/target_two_thresh", 1 );

    squareTargetFound = n.advertise< std_msgs::Time >( "/image_processing/target_one/found", 1 );
    triangleTargetFound = n.advertise< std_msgs::Time >( "/image_processing/target_two/found", 1 );

    // targetOneDetected = n.advertise< std_msgs::Bool > ( "/image_processing/targetOneDetected", 1 );

    // tfbr = tf::TransformBroadcaster();

    ros::Rate loop_rate( 10 );

    ros::spin();

    return 0;
}




// Define all functions

void getThresholding( cv::Mat &src, cv::Mat &thresh, SHAPE_DETECTION shape )
{
    // HSV
    cv::Mat mask;
    if ( shape == SQUARE )
        cv::inRange( src, cv::Scalar( H_ORANGE_LOW, S_ORANGE_LOW, V_ORANGE_LOW ),
                     cv::Scalar( H_ORANGE_HIGH, S_ORANGE_HIGH, V_ORANGE_HIGH ), mask );
    else
        cv::inRange( src, cv::Scalar( H_TRIANGLE_LOW, S_TRIANGLE_LOW, V_TRIANGLE_LOW ),
                     cv::Scalar( H_TRIANGLE_HIGH, S_TRIANGLE_HIGH, V_TRIANGLE_HIGH ), mask );

    cv::bitwise_and( src, src, thresh, mask=mask );
}


void locatingContours( cv::Mat &maskApplied, cv::Mat &src, cv::Mat &edges, std::vector< std::vector< cv::Point > > &contours, std::vector< cv::Vec4i > &hierarchy )
{
    cv::Mat dst;
    cv::Mat maskGray;
    //cv::Mat detectedEdges;
    cv::Mat hsvChannels[3];

    // Convert masked image to gray scale
    cv::split( maskApplied, hsvChannels );
    maskGray = hsvChannels[2];

    // remove some noise
    // cv::blur( maskGray, edges, cv::Size( 3, 3 ) );
    cv::GaussianBlur( maskGray, edges, cv::Size( 3, 3 ), 0 );

    // Detect edges
    cv::Canny( edges, edges, 0, 100, 3 );

    // find the contours
    cv::findContours( edges, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE, cv::Point( 0, 0 ) );

    //dst = cv::Scalar::all( 0 );

    src.copyTo( dst );
}



void drawContours( cv::Mat &detectedEdges, std::vector< std::vector< cv::Point > > &contours, std::vector< cv::Vec4i > &hierarchy, cv::Mat &src, SHAPE_DETECTION shape )
{
    char buffer[18];

    sprintf( buffer, "TARGET %d DETECTED", shape );

    for ( int i = 0; i < contours.size(); i++ )
    {
        cv::Scalar color = cv::Scalar(  0, 255, 0 ); // Green
        cv::drawContours( src, contours, i, color, 2, 8, hierarchy, 0, cv::Point() );
    }

    // calculate moments of shape
    cv::Moments moments;

    moments = cv::moments( contours[0], true );

    cv::Point textOrig;
    textOrig.x = (int)( moments.m10 / moments.m00 ) - 18 * 5;
    textOrig.y = (int)( moments.m01 / moments.m00 );

    cv::putText( src, buffer , textOrig, cv::FONT_HERSHEY_COMPLEX_SMALL,
                 0.8, cv::Scalar( 0, 0, 0 ), 1, CV_AA );
}


bool shapeTargetDetected( std::vector< std::vector< cv::Point > > &contours, cv::Mat &src, std::vector< std::vector< cv::Point > > &shapeContours, SHAPE_DETECTION shape, cv::Point &centre )
{
    // find shapes with contours
    if ( shape == SQUARE )
        findShapesFromContours( contours, shapeContours, 4, AREA_MAX_ORANGE, AREA_MIN_ORANGE, centre );
    else
        findShapesFromContours( contours, shapeContours, 3, AREA_MAX_TRIANGLE, AREA_MIN_TRIANGLE, centre );

    if ( shapeContours.size() > 0 )
        return true;
    else
        return false;
}


void findShapesFromContours( std::vector< std::vector< cv::Point > > &contours, std::vector< std::vector< cv::Point > > &detectedShape, int count, int maxSize, int minSize, cv::Point &centre )
{
    std::vector< std::vector< cv::Point > > temp;
    int area;

    temp.swap( contours );
    contours.clear();

    // locate moments of all contours in image
    for ( int i = 0; i < temp.size(); i++ )
    {

        // check if polygon is the right shape
        std::vector< cv::Point > polygon;
        cv::approxPolyDP( temp[i], polygon, 0.03*cv::arcLength( temp[i], true ), true );

        area = cv::contourArea( temp[i] );

        if ( polygon.size() != count || !( area > minSize && area < maxSize ) )
            continue;

        cv::Moments moments;

        contours.push_back( temp[i] );

        // find centre point
        moments       = cv::moments( temp[i], true );
        centre.x = (int)( moments.m10 / moments.m00 );
        centre.y = (int)( moments.m01 / moments.m00 );

        // find all contours that have the same centre point
        for ( int j = i + 1; j < temp.size(); j++ )
        {
            // check if other contours have the same shape
            std::vector< cv::Point > polygon;
            cv::approxPolyDP( temp[j], polygon, 0.03*cv::arcLength( temp[j], true ), true );
            int currentArea;

            currentArea = cv::contourArea( temp[j] );

            if ( polygon.size() != count || !( currentArea > minSize && currentArea < maxSize ) )
                continue;

            int diffx, diffy;

            cv::Point tempCentre;
            cv::Moments tempMoments;

            tempMoments  = cv::moments( temp[j], true );
            tempCentre.x = (int)( tempMoments.m10 / tempMoments.m00 );
            tempCentre.y = (int)( tempMoments.m01 / tempMoments.m00 );

            diffx    = abs( tempCentre.x - centre.x );
            diffy    = abs( tempCentre.y - centre.y );


            if ( diffx < 6 && diffy < 6 )
                contours.push_back( temp[j] );
        }

        // check if all four contours where found
        if ( detectedShape.size() != 4 )
            contours.clear();
        else
            return;
    }
}


void imageProcessing( cv::Mat &src, cv::Mat &maskSquareSrc, cv::Mat &maskTriangleSrc, cv::Mat &tvec_triangle, cv::Mat &tvec_square, bool &triangle_pnp, bool &square_pnp )
{
    cv::Point centre_square;
    cv::Point centre_triangle;

    cv::Mat squareEdges;
    cv::Mat triangleEdges;

    std::vector< std::vector< cv::Point > > squareContours;
    std::vector< std::vector< cv::Point > > triangleContours;
    std::vector< cv::Vec4i > squareHierarchy;
    std::vector< cv::Vec4i > triangleHierarchy;

    // convert src to hsv Range
    cv::cvtColor( src, src, cv::COLOR_BGR2HSV );

    getThresholding( src, maskSquareSrc, SQUARE );
    getThresholding( src, maskTriangleSrc, TRIANGLE );

    locatingContours( maskSquareSrc, src, squareEdges, squareContours, squareHierarchy );
    locatingContours( maskTriangleSrc, src, triangleEdges, triangleContours, triangleHierarchy );

    cv::cvtColor( maskSquareSrc, maskSquareSrc, cv::COLOR_HSV2BGR );
    cv::cvtColor( src, src, cv::COLOR_HSV2BGR );
    cv::cvtColor( maskTriangleSrc, maskTriangleSrc, cv::COLOR_HSV2BGR );

    // if positive detection draw contours on top to identify target
    if ( shapeTargetDetected( squareContours, src, squareContours, SQUARE, centre_square ) )
    {
        drawContours( squareEdges, squareContours, squareHierarchy, src, SQUARE );
        square_pnp = completeSolvePNP( squareContours, centre_square, SQUARE, src, tvec_square );
    }

    // if positive detection draw contours on top to identify target
    if ( shapeTargetDetected( triangleContours, src, triangleContours, TRIANGLE, centre_triangle ) )
    {
        drawContours( triangleEdges, triangleContours, triangleHierarchy, src, TRIANGLE );
        triangle_pnp = completeSolvePNP( triangleContours, centre_triangle, TRIANGLE, src, tvec_triangle );
    }
}


bool completeSolvePNP( std::vector< std::vector< cv::Point > > &contours, cv::Point &centre, SHAPE_DETECTION shape, cv::Mat &src, cv::Mat &tvec )
{
    std::vector< cv::Point > polygon;
    std::vector< cv::Point2f > points;
    bool solved = false;
    cv::Rect rect;

    // cv::Mat tvec( 3, 1, cv::DataType< double >::type );
    cv::Mat rvec;

    cv::approxPolyDP( contours[0], polygon, 0.03*cv::arcLength( contours[0], true ), true );

    // create bounding rectangle
    rect = boundingRect( cv::Mat( polygon ) );

    cv::Point topLeft     = rect.tl();
    cv::Point bottomRight = rect.br();
    cv::Point topRight    = cv::Point( bottomRight.x, topLeft.y  );
    cv::Point bottomLeft  = cv::Point( topLeft.x,  bottomRight.y );

    // // construct vectors for solve pmp
    // points.push_back( cv::Point2f( ( double )centre.x, ( double )centre.y ) );
    // for ( int i = 0; i < polygon.size(); i++ )
    //     points.push_back( cv::Point2d( ( double )polygon[i].x, ( double )polygon[i].y ) );

    points.push_back( cv::Point2f( ( double )centre.x,      ( double )centre.y      ) );
    points.push_back( cv::Point2f( ( double )bottomLeft.x,  ( double )bottomLeft.y  ) );
    points.push_back( cv::Point2f( ( double )bottomLeft.x,  ( double )centre.y      ) );
    points.push_back( cv::Point2f( ( double )topLeft.x,     ( double )topLeft.y     ) );
    points.push_back( cv::Point2f( ( double )centre.x,      ( double )topLeft.y     ) );
    points.push_back( cv::Point2f( ( double )topRight.x,    ( double )topRight.y    ) );
    points.push_back( cv::Point2f( ( double )topRight.x,    ( double )centre.y      ) );
    points.push_back( cv::Point2f( ( double )bottomRight.x, ( double )bottomRight.y ) );
    points.push_back( cv::Point2f( ( double )centre.x,      ( double )bottomRight.y ) );

    bool success = cv::solvePnP( square_coordinate, points, projection, dist_coefs, rvec, tvec );

    // don't output results on first detection
    if ( shape == SQUARE )
    {
        if ( x_f_square == 0.0 || y_f_square == 0.0 || n_square_frames < NUM_FRAMES || n_square_frames > NUM_FRAMES )
            success = false;

        // compute filtered results
        x_f_square = ( ( 1.0 - alpha ) * x_f_square ) + ( alpha * tvec.at< double >( 0 ) );
        y_f_square = ( ( 1.0 - alpha ) * y_f_square ) + ( alpha * tvec.at< double >( 0 ) );

        n_square_frames++;
    }
    else
    {
        if ( x_f_triangle == 0.0 || y_f_triangle == 0.0 || n_triangle_frames < NUM_FRAMES || n_triangle_frames > NUM_FRAMES  )
            success = false;

        // compute filtered results
        x_f_triangle  = ( ( 1.0 - alpha ) * x_f_triangle ) + ( alpha * tvec.at< double >( 0 ) );
        y_f_triangle  = ( ( 1.0 - alpha ) * y_f_triangle ) + ( alpha * tvec.at< double >( 0 ) );

        n_triangle_frames++;
    }



    // n++;
    //
    // x_f_square = ( x_f_square +

    return success;
}


void imgCallback( const sensor_msgs::Image::ConstPtr &msg )
{
    // Convert image to opencv image
    cv_bridge::CvImagePtr cvPtr;
    cv::Mat img;
    cv::Mat maskSquare;
    cv::Mat maskTriangle;
    cv::Mat tvec_tri;
    cv::Mat tvec_square;
    bool triangle_pnp = false;
    bool square_pnp   = false;



    try
    {
        cvPtr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
        img = cvPtr->image;
    }
    catch ( cv_bridge::Exception &e )
    {
        return;
    }

    // Do image processing
    imageProcessing( img, maskSquare, maskTriangle, tvec_tri, tvec_square, triangle_pnp, square_pnp );

    // add text to the top of the screen indicating that image processing is
    // occuring
    cv::putText( img, "IMAGE PROCESSING OCCURING" , cv::Point( 20, 20 ), cv::FONT_HERSHEY_COMPLEX_SMALL,
                 0.8, cv::Scalar( 0, 0, 0 ), 1, CV_AA );

    // publish output image
    cv_bridge::CvImage outImage;
    outImage.header   = cvPtr->header;
    outImage.encoding = sensor_msgs::image_encodings::BGR8;
    outImage.image    = img;

    outputImage.publish( outImage.toImageMsg() );

    // publish mask image
    cv_bridge::CvImage maskImage;
    maskImage.header   = cvPtr->header;
    maskImage.encoding = sensor_msgs::image_encodings::BGR8;
    maskImage.image    = maskSquare;

    orangeSquareMask.publish( maskImage.toImageMsg() );

    // publish second mask to image
    cv_bridge::CvImage triangleMaskImage;
    triangleMaskImage.header   = cvPtr->header;
    triangleMaskImage.encoding = sensor_msgs::image_encodings::BGR8;
    triangleMaskImage.image    = maskTriangle;

    blueTriangleMask.publish( triangleMaskImage.toImageMsg() );

    // if possible publish transform result
    if ( square_pnp )
    {
        geometry_msgs::TransformStamped msg_trig = geometry_msgs::TransformStamped();
        std_msgs::Time time_msg;
        tf2::Quaternion q;
        q.setRPY( 0, 0, 0 );

        msg_trig.header                  = cvPtr->header;
        msg_trig.child_frame_id          = "square";
        msg_trig.transform.translation.x = -x_f_square;
        msg_trig.transform.translation.y = -y_f_square;
        msg_trig.transform.translation.z = tvec_square.at< double >( 2 );
        msg_trig.transform.rotation.w    = q.w();
        msg_trig.transform.rotation.x    = q.x();
        msg_trig.transform.rotation.y    = q.y();
        msg_trig.transform.rotation.z    = q.z();
      //  msg_trig.header.stamp            = ros::Time::now();
        tfbr->sendTransform( msg_trig );

        time_msg.data = cvPtr->header.stamp;
       // time_msg.data = msg_trig.header.stamp;


        squareTargetFound.publish( time_msg );
    }
    else if ( triangle_pnp )
    {
        std::cout << "transform sent" << std::endl;
        geometry_msgs::TransformStamped msg_trig = geometry_msgs::TransformStamped();
        std_msgs::Time time_msg;
        tf2::Quaternion q;
        q.setRPY( 0, 0, 0 );

        msg_trig.header                  = cvPtr->header;
        msg_trig.child_frame_id          = "triangle";
        msg_trig.transform.translation.x = -x_f_triangle;
        msg_trig.transform.translation.y = -y_f_triangle;
        msg_trig.transform.translation.z = tvec_tri.at< double >( 2 );
        msg_trig.transform.rotation.w    = q.w();
        msg_trig.transform.rotation.x    = q.x();
        msg_trig.transform.rotation.y    = q.y();
        msg_trig.transform.rotation.z    = q.z();
        // msg_trig.header.stamp            = ros::Time::now();
        tfbr->sendTransform( msg_trig );

        time_msg.data = cvPtr->header.stamp;
        // time_msg.data = msg_trig.header.stamp;


        triangleTargetFound.publish( time_msg );
    }
}


void callbackInfo( const sensor_msgs::CameraInfo::ConstPtr &msg )
{
    // get projection matrix
    int row    = 0;
    int column = 0;

    dist_coefs = cv::Mat::zeros( msg->D.size(), 1, cv::DataType< double >::type );

    projection.at< double >( 0, 0 ) = msg->P[0];
    projection.at< double >( 0, 1 ) = msg->P[1];
    projection.at< double >( 0, 2 ) = msg->P[2];
    projection.at< double >( 1, 0 ) = msg->P[4];
    projection.at< double >( 1, 1 ) = msg->P[5];
    projection.at< double >( 1, 2 ) = msg->P[6];
    projection.at< double >( 2, 0 ) = msg->P[8];
    projection.at< double >( 2, 1 ) = msg->P[9];
    projection.at< double >( 2, 2 ) = msg->P[10];

    for ( int i = 0; i < msg->D.size() - 1; i++ )
        dist_coefs.at< double >(i) = msg->D[i];
}
