# Image processing ROS package
## Version 0.0.1

This package utilises colour thresholding and shape detection techniques to analyse and determine if a target has been detected within an image. Currently, the images read from the ROS node are uncompressed images and come from the node /webcam/image_raw. The processing then republishes the results of processing to another node titled /camera/image

---

#### Currently the nodes produced are:
 - /image_processing/output
     - this publishes the final output of the image with the overlay drawn onto the image for the customer to see when a postive detection occurs
 - /image_processing/target_one_thresh
     - This publishes the thresholding image for when the orange square target is being located
 - /image_processing/target_two_thresh
     - This publishes the thresholding image for when the blue triangle target is being located
 - /image_processing/targetOneDetected
     - This node publishes a boolean for any frame where target one is detected
 - /image_processing/targetTwoDetected
     - This node publishes a boolean for any frame where target two is detected
 - /image_processing/target_one_centre_point
     - This node publishes an array of ints, containing the centre point in terms of the pixels. The first element corresponds to the x coordinate, and the second element corresponds to the y coordinage for the first target.
 - /image_processing/target_two_centre_point
     - This node publishes an array of ints, containing the centre point in terms of the pixels. The first element corresponds to the x coordinate, and the second element corresponds to the y coordinage for the second target.

---

#### Installation and Running:

     1. Clone this repository into your catkin workspace in src (~/catkin_ws/src)
     2. Build the package in your catkin workspace directory (in ~/catkin_ws/ run catkin_make)
     3. Source the built packages in your terminal (run: . ~/catkin_ws/devel/setup.bash)
     4. Run the package (assuming roscore, and camera nodes are already running) via: rosrun img_processing image_processing
        - Note that inorder to use the compressed stream while running use the command rosrun img_processing image_processing _image_transport:=compressed
